document.addEventListener('DOMContentLoaded', function() {
    init() 
}, false);


function init() {

    character_select = document.getElementById("character_select")
    for (chara in characters_list) {
        var option = document.createElement("option")
        option.innerHTML = characters_list[chara]
        character_select.add(option)
    }
    character_select.onchange = function() {
        funct()
    }
}


function funct() {

    list_weekly_not_done = document.getElementById("weekly_not_done")
    list_weekly_done = document.getElementById("weekly_done")
    list_daily_not_done = document.getElementById("daily_not_done")
    list_daily_done = document.getElementById("daily_done")

    while (list_weekly_not_done.hasChildNodes()) {
        list_weekly_not_done.removeChild(list_weekly_not_done.firstChild)
    }
    while (list_weekly_done.hasChildNodes()) {
        list_weekly_done.removeChild(list_weekly_done.firstChild)
    }
    while (list_daily_not_done.hasChildNodes()) {
        list_daily_not_done.removeChild(list_daily_not_done.firstChild)
    }
    while (list_daily_done.hasChildNodes()) {
        list_daily_done.removeChild(list_daily_done.firstChild)
    }

    for (var key in weekly) {
        if (weekly[key] == false) {
            var new_li = document.createElement("LI")
            var text = document.createElement("span")
            text.innerHTML = String(key)
            var cross = document.createElement("span")
            var cross_text = document.createTextNode("\u00D7")
            cross.className = "cross"
            cross.onclick = function() {
                classchange(this)
            }
            cross.appendChild(cross_text)
            new_li.appendChild(text)
            new_li.appendChild(cross)
            list_weekly_not_done.appendChild(new_li)
        }
        else {
            var new_li = document.createElement("LI")
            var text = document.createElement("span")
            text.innerHTML = String(key)
            var cross = document.createElement("span")
            var cross_text = document.createTextNode("\u00D7")
            cross.className = "cross"
            cross.onclick = function() {
                classchange(this)
            }
            cross.appendChild(cross_text)
            new_li.appendChild(text)
            new_li.appendChild(cross)
            list_weekly_done.appendChild(new_li)

        }
    }

    for (var key in daily) {
        if (daily[key] == false) {
            var new_li = document.createElement("LI")
            var text = document.createElement("span")
            text.innerHTML = String(key)
            var cross = document.createElement("span")
            var cross_text = document.createTextNode("\u00D7")
            cross.className = "cross"
            cross.onclick = function() {
                classchange(this)
            }
            cross.appendChild(cross_text)
            new_li.appendChild(text)
            new_li.appendChild(cross)
            list_daily_not_done.appendChild(new_li)
        }
        else {
            var new_li = document.createElement("LI")
            var text = document.createElement("span")
            text.innerHTML = String(key)
            var cross = document.createElement("span")
            var cross_text = document.createTextNode("\u00D7")
            cross.className = "cross"
            cross.onclick = function() {
                classchange(this)
            }
            cross.appendChild(cross_text)
            new_li.appendChild(text)
            new_li.appendChild(cross)
            list_daily_done.appendChild(new_li)

        }
    }
};

function classchange(el) {
    item = el.parentNode
    if (item.parentNode == list_weekly_not_done) {
        list_weekly_not_done.removeChild(item)
        list_weekly_done.appendChild(item)
    }
    else if (item.parentNode == list_weekly_done) {
        list_weekly_done.removeChild(item)
        list_weekly_not_done.appendChild(item)
    }
    else if (item.parentNode == list_daily_not_done) {
        list_daily_not_done.removeChild(item)
        list_daily_done.appendChild(item)
    }
    else if (item.parentNode == list_daily_done) {
        list_daily_done.removeChild(item)
        list_daily_not_done.appendChild(item)
    }

    for (var key in weekly) {
        if (el.innerHTML == key) {
            weekly[key] == !weekly[key]
        }
    }
    for (var key in daikly) {
        if (el.innerHTML == key) {
            daikly[key] == !daikly[key]
        }
    }
}
