<?xml version="1.0" ?>
<!DOCTYPE html >
<html xml:lang="en" lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
                <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0" content="height=device-height, initial-scale=1.0"/>
                <title>Arcarox Site</title>

                <link rel="shortcut icon" href="image/balou_favicon.ico" />

                <link rel="stylesheet" href="css/index.css" type="text/css"/>
                <!-- arcarox.online -->
        </head>
        <body class="cancel_animation">
                <header> <!--Fixed header-->
                        <div id="square_left"></div>
                        <h1 class="titre">Arcarox</h1>
                        <h1 class="titre2">The Bear Necessities</br>Il en faut peu pour être heureux</h1>
                        <hr class="ligne" />
                        <div id="square_right"></div>

                </header>
              
                <main>

                        <div class="menu"> <!--Global menu-->
                                <nav class="menu">
                                        <ul class="menu">
                                                <li class="menu_element" id="about">
                                                        <a onclick="dropdown_about()">
                                                                <p id="about_btn_title">About</p>
                                                        </a>
                                                </li>
                                                <li class="menu_element" id="project_btn">
                                                        <a onclick="dropdown_project()">
                                                                <p id="project_btn_title">Projects</p>
                                                        </a>
                                                </li>
                                        </ul>
                                </nav>
                        </div>


                        <div class="hide" id = "projects_list"> <!--List of Gitlab project link or link to web page-->
                                <ul> 
                                        <li class="language">
                                                <strong id="html_title">HTML</strong> <!--HTML project-->
                                                <ul class="list_app">
                                                        <li class="app"> <!--Same css for all projects-->
                                                                <div class="app">
                                                                        <a id = "app_link" href = "pendu/pendu.html" >
                                                                                <strong class="app_title">Hangman</strong>
                                        
                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                Find the word before being hanged  
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>
                                                        
                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id = "app_link" href = "juste_prix/juste_prix.html">
                                                                                <strong class="app_title">The Price Is Right</strong>

                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                Everything is in the name.
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>

                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id = "app_link" href= "my_cook/my_cook.html">
                                                                                <strong class="app_title">My Cook</strong>
                                        
                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                Made the most point in 5 minutes.
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>

                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id = "app_link" href= "try_php/info.php">
                                                                                <strong class="app_title">PHP info</strong>
                                                                        
                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                Finally working.
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>

                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id = "app_link" href= "https://gitlab.com/mailholj/raspberry_pi_web">
                                                                                <strong class="app_title">This Web Site</strong>
                                                                        
                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                Source code of this web site.
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>

                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id = "app_link" href = "reponse_hist/test.html">
                                                                                <strong class="app_title">Reponse</strong>

                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                Pourquoi chercher les réponses sont là.
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>

                                                </ul>
                                        </li>

                                        <li class="language">
                                                <strong id="python_title">Python</strong> <!--Python projects (ofc only gitlab link)-->
                                                <ul class="list_app">
                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id= "app_link" href="https://gitlab.com/mailholj/isn-discord-bot">
                                                                                <strong class="app_title">BOT Discord</strong>
                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                Biggest project in progress.
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>

                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id= "app_link" href="https://gitlab.com/mailholj/isn">
                                                                                <strong class="app_title">ISN</strong>
                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                All the little isn exercises.</br>
                                                                                                And also a Hangman and The Price Is Right in Python.
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>

                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id= "app_link" href="https://gitlab.com/mailholj/bataille_navale_isn">
                                                                                <strong class="app_title">Battleship ISN</strong>
                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                An isn project that is finished but could evolve to multi-player online.
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li>
                                                        <li class="app">
                                                                <div class="app">
                                                                        <a id= "app_link" href="https://gitlab.com/mailholj/projet-final-isn">
                                                                                <strong class="app_title">Slime Adventure - The deadly Journey</strong>
                                                                                <div id="app_info">
                                                                                        <p id="info">
                                                                                                Mario like game made for my final project of ISN
                                                                                        </p>
                                                                                </div>
                                                                        </a>
                                                                </div>
                                                        </li
                                                </ul>
                                        </li>
                                </ul>

                        </div>

                        <div class= "hide" id="about_profil"> <!--Profil part-->

                                <div id="learning"><!--How I learn-->
                                        <p class="about_title" >learning</p>
                                        <p class="about_content">
                                                <strong>I learned a lot by myself.</strong></br>
                                                I learned through personal or school projects, online courses, forum and docs.</br></br></br>
                                                A very simple way to learn coding is to <strong>start project on your own</strong> and and not to be scared by the unknown.</br></br>
                                        </p>
                                        <a class="git_link" href="https://gitlab.com/mailholj">My GitLab profil</a>
                                </div>

                                <div id="rl">
                                        <p class="about_title">real life</p><!--Who I am-->
                                        <p class="about_content">

                                                I'm a 17 years old high school student.</br></br>

                                                Next year I will study at <strong><a href="http://www.epitech.eu/fr/" id="epitech">Epitech Toulouse</a></strong>.</br></br>

                                                I m not athletic, I was playing rugby since the age of 6 but for 3 years I had medical problems that forced me to stop.</br></br>

                                                Of course I like doing things on my computer whether it's coding or playing.</br></br>                                    
                                                But I need to often go out having fun like spending time with my friends.
                                        </p>
	                        </div>

				<div id="contacts">
                                        <p class="about_title">contacts</p>
                                        <div id="list_contacts">
                                                <p id="discord">
                                                        <img class= "logo" src="image/discord_logo.png" alt="https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjC3oeBovXgAhVOJBoKHcOOB7EQjRx6BAgBEAU&url=https%3A%2F%2Ffr.wikipedia.org%2Fwiki%2FFichier%3ADiscord.svg&psig=AOvVaw3xKpLQmvGed3FiGHypvddb&ust=1552227989848857"/>
                                                        <p class="contacts_title">Arcarox#8920</p>
                                                </p>
                                                        
                                                <p id="steam">
                                                        <img class="logo" src="image/steam_logo.jpg" alt="https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiUx4GlovXgAhUMyxoKHXyVBy8QjRx6BAgBEAU&url=https%3A%2F%2Ffr.m.wikipedia.org%2Fwiki%2FFichier%3ASteam_icon_logo.svg&psig=AOvVaw0aevI0YmNx0_PFMyuvpOmq&ust=1552228030998208"/>
                                                        <p class="contacts_title">Arcarox</p>

                                                </p>
                                        </div>
				</div>
                        </div>

                        <div class="balou"> <!--Just two img of Baloo, first used for test but I let them because Baloo-->

                                <img class="balou_img" src="image/balou.jpg" alt="http://ekladata.com/xXx3Jk4mdEKEr1E_ruIl3effYq4.jpg" width="450" height="408"/>
                                <img class="balou_img" src="image/balou.jpg" alt="http://ekladata.com/xXx3Jk4mdEKEr1E_ruIl3effYq4.jpg" width="450" height="408"/>

                        </div>
                </main>

                <footer > <!-- the last part : the footer-->
                        &ltwork in progress> <!-- because it's never finished-->
                </footer>


                <script> /** Just some javascript for the Onclick dropdown and scroll op on load of the page */

                        function dropdown_project() {
                                
                                if (document.getElementById("about_profil").classList.contains("show")){
                                        document.getElementById("about_profil").classList.toggle("show");
                                        document.getElementById("about_profil").classList.toggle("hide");
                                        document.getElementById("about_btn_title").classList.remove("underline");
                                }

                                document.getElementById("projects_list").classList.toggle("show");
                                document.getElementById("projects_list").classList.toggle("hide");
                                document.getElementById("project_btn_title").classList.toggle("underline");
                        }

                        function dropdown_about() {
                                
                                if (document.getElementById("projects_list").classList.contains("show")) {
                                        document.getElementById("projects_list").classList.toggle("show");
                                        document.getElementById("projects_list").classList.toggle("hide");
                                        document.getElementById("project_btn_title").classList.remove("underline");
                                }
                                
                                
                                document.getElementById("about_profil").classList.toggle("show");
                                document.getElementById("about_profil").classList.toggle("hide");
                                document.getElementById("about_btn_title").classList.toggle("underline");
                                
                        }

                        window.onbeforeunload = function () { 
                                window.scrollTo(0, 0);
                        }

                        window.onload = function() {
                                document.getElementsByClassName('cancel_animation')[0].classList.remove("cancel_animation");
                        }
                </script>
                
                <?php 
                        function detectip()
                        {
                        // Récupération de l'ip du visiteur
                        if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                                {
                                        $ip =$_SERVER['HTTP_X_FORWARDED_FOR'];
                                }
                        elseif(isset($_SERVER['HTTP_CLIENT_IP']))
                                {
                                        $ip = $_SERVER['HTTP_CLIENT_IP'];
                                }
                        else
                                {
                                        $ip = $_SERVER['REMOTE_ADDR'];
                                }
                                echo "<script>console.log(".json_encode($ip).")</script>";
                                return $ip;
                        }

                        // ouverture du fichier
                        $fd = fopen ("try_php/ip.log" ,"a+");
                        
                        // récupération de la date et de l'heure
                        $date  = date ("d-m-Y");
                        $heure = date ("H:i");
                        
                        $ip = detectip();
                        
                        // écriture dans le fichier
                        fwrite ($fd,"$ip \r\n");
                        
                        // Fermeture du fichier
                        fclose ($fd);


                ?>
                
        </body>
</html>


